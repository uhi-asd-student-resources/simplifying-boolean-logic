//
//  main.cpp
//  karnaugh-map-test
//
//  Created by Douglas Barr on 05/12/2021.
//

#include <iostream>
#include <chrono>
#include <array>

#include "messyVersion.h"
#include "terseVersion.h"
#include "functionalVersion.h"
#include "tryingToBeFancyVersion.h"
#include "myVersion.h"

int main()
{
    const int numberOfYears = 10000;
    std::array<int, numberOfYears> resultsOfRun = {};
    
    std::cout << std::endl;
    
    {
        auto start = std::chrono::high_resolution_clock::now();

        for(int year = 0; year < numberOfYears; ++year)
        {
            resultsOfRun[year] = isLeapYearMessy(year);
        }
        
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
        std::cout << "The version with nested if statements took \t" << duration.count() << " ms to run." << std::endl;
    }
    
    {
        auto start = std::chrono::high_resolution_clock::now();

        for(int year = 0; year < numberOfYears; ++year)
        {
            resultsOfRun[year] = isLeapYearReallyTerse(year);
        }
        
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
        std::cout << "The REALLY terse version took \t\t\t" << duration.count() << " ms to run." << std::endl;
    }
    
    {
        auto start = std::chrono::high_resolution_clock::now();

        for(int year = 0; year < numberOfYears; ++year)
        {
            resultsOfRun[year] = isLeapYearFunctions(year);
        }
        
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
        std::cout << "The functional version took \t\t\t" << duration.count() << " ms to run." << std::endl;
    }
    
    {
        auto start = std::chrono::high_resolution_clock::now();

        for(int year = 0; year < numberOfYears; ++year)
        {
            resultsOfRun[year] = isLeapYearWithLambdas(year);
        }
        
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
        std::cout << "The version written with Lambdas took \t\t" << duration.count() << " ms to run." << std::endl;
    }
    
    {
        auto start = std::chrono::high_resolution_clock::now();

        for(int year = 0; year < numberOfYears; ++year)
        {
            resultsOfRun[year] = isLeapYearNeat(year);
        }
        
        auto stop = std::chrono::high_resolution_clock::now();
        auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
        std::cout << "My version took \t\t\t\t" << duration.count() << " ms to run." << std::endl;
    }
    
    std::cout << std::endl;
    return 0;
}
